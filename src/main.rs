extern crate nix;
use nix::unistd::*;
use std::env;
use std::fs;
use nix::sys::wait::waitpid;
use std::ffi::CStr;
use std::ffi::CString;



fn main() {
    let program_path = "/usr/bin/rustc";
    let args: Vec<String> = env::args().collect();
    let bin_path = "./".to_string() + args[1].as_str();
    //execve wants a cstr instead
    let cstr_binpath = CStr::from_bytes_with_nul(bin_path.as_bytes()).expect("Conversion to CStr failed");
    let cstr_progpath = CStr::from_bytes_with_nul(program_path.as_bytes()).expect("Conversion to CStr failed");
    let vec_of_cstr: Vec<&CStr> = args
        .iter()
        .map(|s| CString::new(s.as_str()).expect("Conversion to CString failed"))
        .map(|cs| cs.as_c_str())
        .collect();
    

    match fork() {
        Ok(ForkResult::Parent { child }) => {
            // This is the parent process
            match waitpid(child, None) {
                Ok(_) => println!("Child process exited successfully"),
                Err(e) => eprintln!("Failed to wait for child process: {}", e),
            }
        }
        Ok(ForkResult::Child) => {
            // This is the child process
            match execve(cstr_progpath, &[], &[] /* environment variables */) {
                Ok(_) => unreachable!("execve should never return if successful"),
                Err(e) => eprintln!("Failed to execute the program: {}", e),
            }
        }
        Err(e) => eprintln!("Fork failed: {}", e),
    }
    // upper is compile now I need to run and delete the bin
    match fork(){
        Ok(ForkResult::Parent{child})=> {
            //parent
            match waitpid(child, None) {
                Ok(_) => println!("Child process exited successfully"),
                Err(e) => eprintln!("Failed to wait for child process: {}", e),
            }
        }
        Ok(ForkResult::Child) => {
            // This is the child process
            match execve(cstr_binpath, &[], &[] /* environment variables */) {
                Ok(_) => unreachable!("execve should never return if successful"),
                Err(e) => eprintln!("Failed to execute the program: {}", e),
            }
        }
        Err(e) => eprintln!("Fork failed: {}", e),
    }
    match fs::remove_file(bin_path) {
        Ok(_) => {
            println!("File '{}' has been successfully deleted.", bin_path);
        }
        Err(e) => {
            eprintln!("Error deleting the file: {}", e);
        }
    }
}
